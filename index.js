// console.log("Reading List Activity 1")

// Activity Template:
// (Copy this part on your template)


/*
	1.) Create a function that returns a passed string with letters in alphabetical order.
		Example string: 'mastermind'
		Expected output: 'adeimmnrst'
	
*/

// Code here:

function alphaOrder(str)
  {
return str.split('').sort().join('');
  }

console.log("Result of Activitiy Part 1:")  
console.log(alphaOrder("mastermind"));


/*
	2.) Write a simple JavaScript program to join all elements of the following array into a string.

	Sample array : myColor = ["Red", "Green", "White", "Black"];
		Expected output: 
			Red,Green,White,Black
			Red,Green,White,Black
			Red+Green+White+Black

*/

// Code here:

console.log(" ");


myColor = ["Red", "Green", "White", "Black"];

console.log("Result of Activitiy Part 2:")  
console.log(myColor.toString());
console.log(myColor.join());
console.log(myColor.join('+'));


/*
	3.) Write a function named birthdayGift that pass a gift as an argument.
		- if the gift is a stuffed toy, return a message that says: "Thank you for the stuffed toy, Michael!"
		- if the gift is a doll, return a message that says: "Thank you for the doll, Sarah!"
		- if the gift is a cake, return a message that says: "Thank you for the cake, Donna!"
		- if other gifts return a message that says: "Thank you for the (gift), Dad!"
		- create a global variable named myGift and invoke the function in the variable.
		- log the global var in the console

*/

// Code here:

console.log(" ");

function birthdayGift(gift){

	switch (gift) {
		case 'stuffed toy' :
			console.log("Thank you for the " + gift + ", Michael!")
			break;

		case 'doll'	:
			console.log("Thank you for the " + gift + ", Sarah!")
			break;

		case 'cake' :
			console.log("Thank you for the " + gift + ", Donna!")
			break;	

		default:
			console.log("Thank you for the " + gift + ", Dad!")
			break;	
	}
}

console.log("Result of Activitiy Part 3:") 
let theGift = birthdayGift('stuffed toy');
birthdayGift('doll');
birthdayGift('cake');
birthdayGift('Laptop');

/*
	4.) Write a function that accepts a string as a parameter and counts the number of vowels within the string.

		Example string: 'The quick brown fox'
		Expected Output: 5

*/

// Code here:
console.log(" ");


function countVowel(str) { 

    let count = str.match(/[aeiou]/gi).length;
    return count;
}

let sampleProb = "The quick brown fox";
let result = countVowel(sampleProb);

console.log("Result of Activitiy Part 4:") 
console.log(result);